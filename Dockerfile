FROM python:3

RUN mkdir app

COPY ./ ./app

WORKDIR /app

RUN pip3 install --no-cache-dir -r ./requirements.txt

ENTRYPOINT ["python3", "main.py"]
