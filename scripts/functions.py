"""
Файл с функциями
"""
import regex as re
import pandas as pd
import warnings
import gitlab
GUEST = gitlab.GUEST_ACCESS
OWNER = gitlab.OWNER_ACCESS
REPORTER = gitlab.REPORTER_ACCESS
MAINTAINER = gitlab.MAINTAINER_ACCESS
DEVELOPER = gitlab.DEVELOPER_ACCESS


def check_names(group, subgroup, type_work):
    """
    Проверка правильного ввода перменных
    Args:
        group - название группы
        subgroup - название подгруппы
        type_work - тип функции

   Warnings:
        ValueError - ввод некорректных данных

    """
    if type_work == 'group':
        if not group:
            raise ValueError("Название группы не может быть пустым! ")
        if re.search(r'[а-яА-Я]', group):
            raise ValueError("Нельзя использовать русские буквы в группах! ")
        if subgroup and re.search(r'[а-яА-Я]', subgroup):
            raise ValueError("Нельзя использовать русские буквы в группах! ")
        return
    if type_work == 'sub':
        if not group or not subgroup:
            raise ValueError("Названия не могут быть пустыми! ")

        if re.search(r'[а-яА-Я]', group) \
                or re.search(r'[а-яА-Я]', subgroup):
            raise ValueError("Нельзя использовать русские буквы в "
                             "предмете или ветке! ")
        return


def protect_group(name):
    # pylint: disable=invalid-name
    """
    Защита группы от удаления
    Args:
        name(str) - название защищаемой группы

    Returns:
        True

    """
    data = [name]

    df = pd.DataFrame(data)
    df.to_csv(r'./data/groups.csv', mode='ab', header=False, index=False)
    return True


def check_secure(secured, group):
    """
    Проверка на защиту
    Args:
        secured - бит защиты
        group - название группы

    Returns: None

    """
    if secured is None:
        raise UserWarning('Вы не выбрали параметр защищенности группы')

    if secured is True:
        protect_group(group)


def get_by_id(self):
    """
        Получение группы как обЪекта по id
    """
    if self._id is not None:
        group = self._git.groups.get(self._id)
        return group
    return False


def remove_rudiments(groups, group_name):
    """
    Удаляем ненужные группы вида 'group_name / какой-то треш'
    Args:
        groups - list групп
        group_name - название необходимой группы

    Returns:
        list очищенных групп

    """
    result = []
    for i in groups[:]:
        if i.name == group_name.rpartition('/ ')[2]:
            result.append(i)
    return result


def check_len(groups):
    """
    Проверка на количество групп
    Args:
        groups - list групп

    Returns:
        None

    """
    if len(groups) > 1:
        string = '\n'
        names = [f"{i.full_name} ; " for i in groups]
        result = string.join(names)
        raise ValueError(f"Количество групп больше одной. "
                         f"Введите полный путь в формате 'group / subgroup'."
                         f" Найденные группы: {result}")


def check_group(name):
    """
    Проверка на присутствие группы в файле защищенных групп
    Args:
        name(str) - Название группы

    Returns:
        True - Название присутствует
        False - Название отсутствует

    """
    try:
        file = pd.read_csv("../data/groups.csv", sep=";")
        res = file[file['groups'].isin([name])]
        if res.empty:
            return False
    except pd.errors.EmptyDataError:
        return False
    return True


def add_to_proj(project, user_id, fullname):
    # pylint: disable=invalid-sequence-index
    """
    Добавление слушателей в группу и проект на сервисе git
    с правами DEVELOPER
    Args:
        fullname: - имя пользователя
        project - проект, в который добавляем пользователей
        user_id - id пользователя
    Warnings:
                GitlabCreateError - ошибка добавления
    """
    try:
        project.members.create({'user_id': user_id,
                                'access_level': DEVELOPER})
    except gitlab.exceptions.GitlabCreateError as err_c:
        warnings.warn(f"Ошибка добавления {fullname} в проект, {err_c.args[0]}")
    return True
