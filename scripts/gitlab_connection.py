
"""
Описан класс работы с сервисом git
"""
# pylint: disable = redefined-builtin
import time
import warnings
import pandas as pd
from scripts import functions as func
import gitlab
from requests.exceptions import ConnectionError

GUEST = gitlab.GUEST_ACCESS
OWNER = gitlab.OWNER_ACCESS
REPORTER = gitlab.REPORTER_ACCESS
MAINTAINER = gitlab.MAINTAINER_ACCESS
DEVELOPER = gitlab.DEVELOPER_ACCESS
SITE = 'https://gitlab.com/'


# pylint: disable=too-many-instance-attributes
class Connection:
    """ Connection class """

    def __init__(self):

        """
        init для класса работы с сервисом git
        """
        self._git = None
        self._token = None
        self._group_name = None
        self._group = None
        self._subgroup = None
        self._subgroup_name = None
        self._file_prepods = None
        self._file_users = None
        self._kidp = None
        self._id = None
        self._milestone = None
        self._result = None
        self._user = None
        self._role = None
        self._file_groups = None

    def _set_role(self, role):
        self._role = role

    def _set_user(self, user):
        self._user = user

    def _set_group_ob(self, group):
        self._group = None
        for one_group in self._git.groups.list():
            if one_group.name == group:
                self._group = one_group

    def _set_file_groups(self, file_groups):
        try:
            self._file_groups = pd.read_csv(file_groups, sep=";")
        except FileNotFoundError as err_f:
            raise FileNotFoundError(f"Не найден файл {err_f.filename}") from err_f

    def _ret_group_name(self):
        return self._group_name

    def _ret_subgroup_name(self):
        return self._subgroup_name

    def ret_git(self):
        """
        Метод возвращающий обЪект гита
        """
        return self._git

    def set_token(self, token):
        """
        Метод устанавливающий токен
        """
        self._token = token

    def set_gitlab(self, token):
        """
        Производит авторизацию по токену и возвращает
            объект соединения с сервисом git

            Returns:
                git: объект соединения с сервером
                err: сообщение об ошибке авторизации
        """
        self.set_token(token)
        try:
            self._git = gitlab.Gitlab(SITE, private_token=self._token)
            self._git.auth()

        # pylint: disable = redefined-builtin
        except ConnectionError as err_c:
            raise ConnectionAbortedError(f'Ошибка подключения к сайту {SITE}') from err_c
        except gitlab.exceptions.GitlabAuthenticationError as err_a:
            raise gitlab.GitlabAuthenticationError('Ошибка аутентификации') from err_a
        return self._git

    def _set_group(self, group):
        """
        Установка объекта группы для облегчения работы с ним
        Args:
            group: объект группы
        """
        self._group = group

    def set_add_g(self, group, file_prepods, file_users, subgroup=None):
        """
        Установка параметров для облегчения работы с функцией создания группы
        Args:
            group - название группы
            file_prepods - файл с преподавателями
            file_users - файл с пользователями
            subgroup - название подгруппы
        """
        func.check_names(group, subgroup, 'group')

        self._group_name = group
        self._subgroup_name = subgroup
        try:
            prepods = pd.read_csv(file_prepods, sep=";")
            users = pd.read_csv(file_users, sep=";")

        except FileNotFoundError as err_f:
            raise FileNotFoundError(f"Не найден файл {err_f.filename}") from err_f
        self._file_prepods = prepods
        self._file_users = users

    def _set_del_g(self, group, subgroup=None):
        """
        Установка параметров для облегчения работы с функцией удаления группы
        Args:
            group - название группы
            subgroup - название подгруппы
        """
        func.check_names(group, subgroup, 'group')
        self._group_name = group
        self._subgroup_name = subgroup

    def _set_add_s(self, sub_name, group_name, file_prepods):
        """
        Установка параметров для облегчения работы с функцией создания подгрупп предметов
        Args:
            group_name - название группы
            file_prepods - файл с преподавателями
            sub_name - название подгруппы
        Warnings:
                 FileNotFoundError - ошибка отсутствия файла
        """
        func.check_names(group_name, sub_name, 'sub')
        self._group_name = group_name
        self._subgroup_name = sub_name
        try:
            self._file_prepods = pd.read_csv(file_prepods, sep=",")
        except FileNotFoundError as err_f:
            raise FileNotFoundError(f"Не найден файл {err_f.filename}") from err_f

    def _set_del_m(self, group, file_users, subgroup=None):
        """
        Установка параметров для облегчения работы с функцией удаления подгрупп, предметов
        Args:
            group - название группы
            file_users - файл с пользователями
            subgroup - название подгруппы
        Warnings:
                 FileNotFoundError - ошибка отсутствия файла
        """
        func.check_names(group, subgroup, 'group')
        self._group_name = group
        self._subgroup_name = subgroup
        try:
            self._file_users = pd.read_csv(file_users, sep=";")
        except FileNotFoundError as err_f:
            raise FileNotFoundError(f"Не найден файл {err_f.filename}") from err_f

    def set_add_o(self, milestone, kidp, result, group_id=None):
        """
        Установка параметров для облегчения работы с функцией создания групп
        Args:
            milestone - временная метка
            kidp - kidp в группе
            result -  группа для сдачи предмета
            group_id - индивидуальный номер группы
        Warnings:
                 ValueError - ошибка отсутствия параметров
        """
        if kidp is None:
            raise ValueError("Вы не выбрали, kidp-группа или нет!")
        if bool(int(kidp)) is True:
            self._kidp = True
        if milestone:
            try:
                time.strptime(milestone, '%Y-%m-%d')
                self._milestone = milestone
            except ValueError as err:
                raise ValueError("Формат milestone должен быть 'ГГГГ-ММ-ДД'! ") from err
        if result is None:
            raise ValueError("Вы не выбрали, группа для сдачи предмета или нет!")
        if bool(int(result)) is True:
            self._result = True
        self._id = group_id

    def result_group(self):
        """
        Привязка группы к result

        Warnings:
                 IndexError - ошибка добавления
                 GitlabCreateError -ошибка создания группы
        """
        if self._result is True:
            try:
                group = self._git.groups.list(search='results')[0]
                self._git.groups.create({'name': self._group_name,
                                         'path': self._group_name,
                                         'parent_id': group.id})
            except gitlab.exceptions.GitlabCreateError:
                gitlab.exceptions.GitlabCreateError(f"Подгруппа {self._group_name} уже существует")
            except IndexError as err:
                raise IndexError("Группа results не найдена") from err
            return True
        return False

    def create_proj(self):
        """
        Создание проектов для заданного списка слушателей, а также
        добавления их в группу и проект

        Warnings:
                 IndexError - ошибка добавления
                 GitlabCreateError -ошибка создания группы

        """
        for i, j in zip(self._file_users.fullname, self._file_users.login):
            try:
                # Добавляем пользователя в группу при необходимости
                user = self._git.users.list(username=j)[0]
                # Создаём проект и добавляем в него
                project = self._git.projects.create({'name': i, 'namespace_id': self._group.id})
                # add_to_proj(project, user.id, i)
                project.members.create({'user_id': user.id,
                                        'access_level': DEVELOPER})
                # Аналогично в случае проекта kidp
                if self._kidp:
                    self.make_milestone(project, i)
                    project_dev = self._git.projects.create({'name': f'{i}-dev',
                                                             'namespace_id': self._group.id})
                    project_dev.members.create({'user_id': user.id,
                                                'access_level': DEVELOPER})
                    # add_to_proj(project_dev, user.id, i)
            except IndexError as err_i:
                raise IndexError(f"Пользователь {i} не найден и не добавлен в members") from err_i
            except gitlab.exceptions.GitlabCreateError as err_c:
                gitlab.exceptions.GitlabCreateError(f"Ошибка создания проекта {i} в группе,"
                                                    f" {err_c.args[0]}")

    def _get_subgroup(self, group, subgroup_name):
        """
            Получение группы как объекта
            Args:
                group - группа как обЪект
                subgroup_name - название подгруппы
            Warnings:
                    IndexError - ошибка добавления подгруппы
            Returns:
             Объект группы
        """
        try:
            subgroup = group.subgroups.list(search=subgroup_name)[0]
            self._subgroup = self._git.groups.get(subgroup.id, lazy=True)
        except IndexError as err_i:
            raise IndexError(f'Не найдена группа {subgroup_name}') from err_i
        return self._subgroup

    def add_prepods(self):
        """
            Добавление преподавателей на сервис git
        Warnings:
                 IndexError - ошибка отсутствия преподавателя
                 GitlabCreateError -ошибка добавления в группу
        """
        for j in self._file_prepods.login:
            try:
                user = self._git.users.list(username=j)[0]
                self._group.members.create({'user_id': user.id, 'access_level': OWNER})

            except IndexError as err_i:
                raise IndexError(f"Преподаватель {j} не найден и не добавлен в members") from err_i
            except gitlab.GitlabCreateError:
                gitlab.GitlabCreateError(f"Преподаватель {j} уже добавлен"
                                         f" в members")

    def add_to_groups(self, user, role, file_groups_name):
        """
           Добавление пользователя в список проектов
           Args:
                user - имя пользователя
                file_groups_name - файл с группами
                role - роль для добавления

           Warnings:
                GitlabCreateError - ошибка добавления пользователя в проект
                NameError - обращение к несуществующей группе
                IndexError - поиск несуществующего пользователя
                FileNotFoundError - ошибка отсутствия файла
                AttributeError - ошибка образения к несуществующему обЪекту
                Warning - вызывается в случае успешного выполнения
           Returns: результат работы
        """
        choice = {"OWNER": '50', "MAINTAINER": '40', "DEVELOPER": '30',
                  "REPORTER": '20', "GUEST": '10'}
        self._set_user(user)
        self._set_role(role)
        try:
            self._set_file_groups(file_groups_name)
            if self._role.upper() in choice:
                for i in self._file_groups.name:
                    self._set_group_ob(str(i))
                    if self._group is None:
                        raise NameError
                    for one_user in self._git.users.list(username=self._user):
                        if one_user.username == self._user:
                            self._user = one_user
                    self._group.members.create({'user_id': self._user.id,
                                                'access_level': choice[self._role.upper()]})
            else:
                raise ValueError("Неверное название роли")
        except IndexError as err_i:
            raise IndexError(f"Пользователь {self._user} не найден и не добавлен") from err_i
        except gitlab.GitlabCreateError as err_c:
            raise gitlab.GitlabCreateError(f"Пользователь {self._user} "
                                           f"уже добавлен в группу {i}") from err_c
        except NameError as err_n:
            raise NameError(f"Неверная группа {self._group}") from err_n
        except AttributeError as err:
            raise AttributeError(f"Группа или пользователь не существуют {err}") from err
        except FileNotFoundError as err_f:
            raise FileNotFoundError(f"Не найден файл {err_f.filename}") from err_f
        raise Warning("Успешное выполнение")

    def change_role(self, user, group, role):
        """
           Изменеие роли продьзователя в пректе
           Args:
                user - имя пользователя
                group - группа для добавления
                role - роль
           Warnings:
                AttributeError - ошибка с обращения к несущсвующей группе или пользователю
                IndexError - ошибка обращения пользователю, не находящемуся в группе
                ValueError - выбор несуществующей роли
                Warning - вызывается в случае успешного выполнения
           Returns: результат работы
        """
        choice = {"OWNER": '50', "MAINTAINER": '40', "DEVELOPER": '30',
                  "REPORTER": '20', "GUEST": '10'}
        self._set_role(role)
        self._set_user(user)
        self._set_group_ob(group)
        try:
            if self._role.upper() in choice:
                member = self._group.members.get(self._git.users.list(username=self._user)[0].id)
                member.access_level = choice[self._role.upper()]
                member.save()
            else:
                raise ValueError("Неверное название роли")
        except IndexError as err_i:
            raise IndexError(f"Пользователь {self._user} не найден и не изменена роль "
                             f"для группы {self._group.name}") from err_i
        except AttributeError as err_a:
            raise AttributeError("Неверная группа или пользователь") from err_a
        finally:
            self._group = None
        raise Warning("Успешное выполнение")

    def add_to_group(self, user_id, fullname):
        """
        Добавление слушателей в группу на сервисе git
        с правами GUEST
        Args:
            fullname - имя пользователя
            user_id - id пользователя
        Warnings:
            GitlabCreateError - ошибка добавления
        """
        try:
            self._group.members.create({'user_id': user_id,
                                        'access_level': GUEST})
        except gitlab.exceptions.GitlabCreateError as err_c:
            gitlab.exceptions.GitlabCreateError(f"Ошибка добавления {fullname} "
                                                f"в группу  {err_c.args[0]}")

    # Создание milestone вводится в формате ГГГГ-ММ-ДД
    def make_milestone(self, project, title):
        """
        Создание вехи.
        Args:
            project - Объект проекта
            title - fullname пользователя
        Warnings:
            GitlabCreateError - ошибка создания milestone
        """
        try:
            project.milestones.create({'title': title, 'due_date': self._milestone})
        except gitlab.GitlabCreateError as err:
            warnings.warn(f"Ошибка создания milestone в группе {project.name},  {err.args[0]}")

    def _delete_users(self, group):
        for user in self._file_users.login:
            try:
                group.members.delete(self._git.users.list(username=user)[0].id)
            except gitlab.exceptions.GitlabDeleteError as err_d:
                raise gitlab.exceptions.GitlabDeleteError(
                    f"Пользователя {user} нет в {self._group_name}") from err_d
            except IndexError as err_i:
                raise IndexError(f"Пользователь {user} не найден") from err_i

    def delete_members(self, group_name, file_users, subgroup=None):
        """
               Удаление пользователей
           Args:
               group_name - Название группы
               file_users - файл с пользователями
               subgroup - название подгруппы
           Returns:
               Возвращает сообщение об успешности функции, в ином случае сообщение об ошибке.
           Raises:
               SystemError - проверка на защиту******
               gitlab.GitlabAuthenticationError*****
               gitlab.exceptions.GitlabDeleteError
               IndexError - вызывается, если не найдена группа или подруппа
               Warning - вызывается в случае успешного выполнения
           """
        self._set_del_m(group_name, file_users, subgroup)
        self._set_group_ob(group_name)
        group_name = self._ret_group_name()
        git = self.ret_git()
        try:
            groups = git.groups.list(search=group_name, order_by='id', sort='desc')
            groups = func.remove_rudiments(groups, group_name)
            func.check_len(groups)
            group = groups[0]
        except IndexError as err:
            raise IndexError(f"{group_name} не найден") from err
        self._delete_users(group)
        raise Warning(f"Пользователи удалены успешно из {group_name}")

    def delete_group(self, group_name, subgroup_name=None):
        """
            Удаление группы или подгруппы
        Args:
            git - объект для работы с сервисом git
        Returns:
            Возвращает сообщение об успешности функции, в ином случае сообщение об ошибке.
        Raises:
            SystemError - проверка на защиту
            gitlab.exceptions.GitlabDeleteError - Ошибка удаления группы
            IndexError - вызывается, если не найдена группа
            Warning - вызывается в случае успешного выполнения
        """
        self._set_del_g(group_name, subgroup_name)
        group_name = self._ret_group_name()
        subgroup_name = self._ret_subgroup_name()
        git = self.set_gitlab(self._token)
        if func.check_group(group_name):
            raise SystemError('Группа защищена')
        try:
            groups = git.groups.list(search=group_name, order_by='id')
            groups = func.remove_rudiments(groups, group_name)
            func.check_len(groups)
            group = groups[0]
        except IndexError as err:
            raise IndexError(f"{group_name} не найден") from err
        try:
            if not subgroup_name:
                group.delete()
                raise Warning(f"Группа {group_name} удалена успешно")
            subgroup = group.subgroups.list(search=subgroup_name, order_by='id')[0]
            git.groups.get(subgroup.id, lazy=True).delete()
            raise Warning(f"Группа {subgroup_name} удалена успешно")
        except gitlab.exceptions.GitlabDeleteError as err:
            raise gitlab.GitlabDeleteError("Ошибка удаления группы") from err
        except IndexError as err:
            raise IndexError(f"{subgroup_name} не найден") from err

    def create_group_and_proj(self):
        """ Создание групп
        В файле должен быть столбец login, который содержит логины преподавателей.
        Каждый преподаватель будет добавлен в группу с правами "owner".
        В файле должен быть столбец login и fullname,
        которые содержат логины слушателей и их полные имена.
        Для каждого слушателя будет создан проект с названием <fullname>,
        он будет добавлен в этот проект с правами "developer":
        Args:
            git_ob - объект для работы с сервисом git
        Returns:
            Возвращает сообщение об успешности функции, в ином случае сообщение об ошибке.
        Raises:
            gitlab.exceptions.GitlabCreateError - ошибка создания группы
            indexError - ошибка создания или поиска группы
            Warning - вызывается в случае успешного выполнения
        """
        # Получение необходимых элементов
        git = self.ret_git()
        group_name = self._ret_group_name()
        subgroup_name = self._ret_subgroup_name()
        try:
            groups = git.groups.list(search=group_name)
            # Удалим вложенные группы, проверим количество подходящих
            groups = func.remove_rudiments(groups, group_name)
            func.check_len(groups)
            group = groups[0]
        except IndexError:
            try:
                group = git.groups.create({'name': group_name, 'path': group_name})
            except gitlab.exceptions.GitlabCreateError as err:
                print(err)
                raise IndexError(f"Ошибка создания или поиска группы {group_name}. "
                                 f" Создайте вручную и вызовите повторно функцию.") from err
        self._set_group(group)
        self.add_prepods()
        if not subgroup_name:
            self.create_proj()
            raise Warning("Успешное выполнение")
        # Работа с подгруппой
        try:
            git.groups.create({'name': subgroup_name, 'path': subgroup_name,
                               'parent_id': group.id})
        except gitlab.exceptions.GitlabCreateError as err:
            warnings.warn(f"Подгруппа {subgroup_name} уже существует", ResourceWarning)
        subgroup = self._get_subgroup(group, subgroup_name)
        self._set_group(subgroup)
        self.create_proj()
        raise Warning("Успешное выполнение")

    def create_sub(self, sub_name, group_name, file_prepods):
        """
        Args:
            self: объект для работы с сервисом git
            sub_name - название предмета
            group_name - название групы
            file_prepods - файл с преподавателями
        Returns:
            Возвращает сообщение об успешности функции, в ином случае сообщение об ошибке.
        Raises:
            IndexError - если не найдена группа
            ValueError - если количество удовлетворяющих поиску групп > 1
            Warning - вызывается в случае успешного выполнения
        """
        self. _set_add_s(sub_name, group_name, file_prepods)
        # Получение родительского каталог sub/int
        try:
            groups = self._git.groups.list(search=self._group_name, order_by='id', sort='desc')
            if len(groups) > 1:
                for i in groups:
                    if i.name == self._group_name:
                        project = i
            else:
                group = groups[0]

                # group в этом случае - созданная подгруппа
                project = self._git.groups.create({'name': sub_name, 'path': sub_name,
                                                   'parent_id': group.id})
            # Добавление преподавателей в subject
            self._set_group(project)
            self.add_prepods()
        except gitlab.exceptions.GitlabCreateError as err:
            raise gitlab.exceptions.GitlabCreateError(
                f"Подгруппа {sub_name} уже существует") from err
        except IndexError as err:
            raise gitlab.GitlabSearchError(f"{group_name} не найден") from err
        raise Warning("Успешное выполнение")




