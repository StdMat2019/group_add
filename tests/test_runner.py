"""
Обработка аргументов для тестирования проекта в CLI
"""
import unittest
import argparse
# pylint: disable=import-error
from tests import test_integrations

if __name__ == '__main__':
    PARSER = argparse.ArgumentParser(description='Test_GitWork_grouper')
    PARSER.add_argument('--token', '-t', type=str)
    PARSER.add_argument('--connection', action='store_true')
    PARSER.add_argument('--group_add', action='store_true')
    PARSER.add_argument('--sub_add', action='store_true')
    PARSER.add_argument('--group_del', action='store_true')
    PARSER.add_argument('--group_delm', action='store_true')
    PARSER.add_argument('--args', action='store_true')
    PARSER.add_argument('--groups_add', action='store_true')
    PARSER.add_argument('--chrole', action='store_true')
    PARSER.add_argument('--all', action='store_true')
    ARGS = PARSER.parse_args()

    if not ARGS.token:
        raise Exception("No token")
    MainTestSuite = unittest.TestSuite()
    test_integrations.get_token(ARGS.token)
    if ARGS.connection or ARGS.all:
        MainTestSuite.addTest(unittest.makeSuite(test_integrations.TestConnection))
    if ARGS.group_add or ARGS.all:
        MainTestSuite.addTest(unittest.makeSuite(test_integrations.TestAddGroup))
    if ARGS.group_del or ARGS.all:
        MainTestSuite.addTest(unittest.makeSuite(test_integrations.TestDeleteGroup))
    if ARGS.sub_add or ARGS.all:
        MainTestSuite.addTest(unittest.makeSuite(test_integrations.TestAddSubject))
    if ARGS.group_delm or ARGS.all:
        MainTestSuite.addTest(unittest.makeSuite(test_integrations.TestDeleteMember))
    if ARGS.groups_add or ARGS.all:
        MainTestSuite.addTest(unittest.makeSuite(test_integrations.TestAddToGroups))
    if ARGS.chrole or ARGS.all:
        MainTestSuite.addTest(unittest.makeSuite(test_integrations.TestChangeRole))

    runner = unittest.TextTestRunner(verbosity=2)
    runner.run(MainTestSuite)
