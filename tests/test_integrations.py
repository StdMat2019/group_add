"""
Тесты для программы Grouper
"""
import unittest
import pandas as pd
import gitlab
# pylint: disable=import-error, missing-function-docstring
from scripts.gitlab_connection import Connection
from scripts.functions import *

TOKEN_ = 'nW85gp96SFoLqkqNNRSG'
TEST_PREPOD_NAME = "mul4an"  # имя должено отличатся от имени тестирующего


def get_token(token_com):
    # pylint: disable=global-statement
    global TOKEN_
    TOKEN_ = token_com


class TestConnection(unittest.TestCase):
    """
    Тест проверки содинения
    """
    def setUp(self):
        self.connection = Connection()
        self.connection.set_token(TOKEN_)

    def test_connect(self):
        self.assertIsNotNone(self.connection.set_gitlab(TOKEN_))

    def test_wrong_token(self):
        wrong_token = 'WrongToken'
        with self.assertRaises(gitlab.GitlabAuthenticationError):
            self.connection.set_gitlab(wrong_token)

    def test_wrong_name_g(self):
        group = 'Русский'
        subgroup = None
        file_users = "../csv_test/test1_u.csv"
        file_prepods = "../csv_test/test1_p.csv"
        with self.assertRaises(ValueError
                               ) as context:
            self.connection.set_add_g(group, file_prepods, file_users, subgroup)
        self.assertTrue('Нельзя использовать русские '
                        'буквы в группах' in str(context.exception))

    def test_wrong_name_s(self):
        group = 'right'
        subgroup = 'Русский'
        file_users = "../csv_test/test1_u.csv"
        file_prepods = "../csv_test/test1_p.csv"
        with self.assertRaises(ValueError
                               ) as context:
            self.connection.set_add_g(group, file_prepods, file_users, subgroup)
        self.assertTrue('Нельзя использовать русские '
                        'буквы в группах' in str(context.exception))

    def test_none_name_s(self):
        group = None
        subgroup = 'Русский'
        file_users = "../csv_test/test1_u.csv"
        file_prepods = "../csv_test/test1_p.csv"
        with self.assertRaises(ValueError
                               ) as context:
            self.connection.set_add_g(group, file_prepods, file_users, subgroup)
        self.assertTrue('Название группы не может быть пустым' in str(context.exception))

    def test_wrong_csv(self):
        group = 'ok'
        subgroup = 'ok'
        file_users = "csv_test/test10765_u.csv"
        file_prepods = "csv_test/test18493_p.csv"
        with self.assertRaises(FileNotFoundError
                               ) as context:
            self.connection.set_add_g(group, file_prepods, file_users, subgroup)
        self.assertTrue(f'Не найден файл {file_prepods}' in str(context.exception))

    def test_check_secure(self):
        with self.assertRaises(UserWarning
                               ) as context:
            check_secure(None, "Right_group123141")
        self.assertTrue('Вы не выбрали параметр защищенности группы' in str(context.exception))


class TestAddGroup(unittest.TestCase):
    """
    Тестирование функции group_add.
    Также происходит добавление пользователей и преподавателей в members
    """
    def setUp(self):
        self.connection = Connection()
        self.connection.set_gitlab(TOKEN_)
        self.git = self.connection.ret_git()

    def test_add_group(self):
        self.setUp()
        file_users = "../csv_test/test1_u.csv"
        file_prepods = "../csv_test/test1_p.csv"
        group_name = "Right_group_Name_2130972325467"  # нужно создать группу
        self.connection.set_add_g(group_name, file_prepods, file_users)
        with self.assertRaises(Warning) as context:
            self.connection.create_group_and_proj()

        self.assertTrue('Успешное выполнение' in str(context.exception))
        self.assertWarns(UserWarning)

        group = self.git.groups.list(search=group_name)[0]
        group.delete()

    def test_add_exist_group(self):
        self.setUp()
        file_users = "../csv_test/test1_u.csv"
        file_prepods = "../csv_test/test1_p.csv"
        group_name = "Exist_group_Name_2130484272009"  # нужно создать группу
        self.connection.set_add_g(group_name, file_prepods, file_users)

        with self.assertRaises(Warning
                               ) as context:
            self.connection.create_group_and_proj()
            self.connection.create_group_and_proj()
        self.assertTrue('Успешное выполнение' in str(context.exception))
        self.assertWarns(ResourceWarning)

        group = self.git.groups.list(search=group_name)[0]
        group.delete()

    def test_add_sub(self):
        self.setUp()
        file_users = "../csv_test/test1_u.csv"
        file_prepods = "../csv_test/test1_p.csv"
        group_name = "Right_group_Name_2130547383658"  # нужно создать группу
        subgroup_name = "Right_subgroup_Name_2130484272"
        self.connection.set_add_g(group_name, file_prepods,
                                  file_users, subgroup_name)
        with self.assertRaises(Warning) as context:
            self.connection.create_group_and_proj()
        self.assertTrue('Успешное выполнение' in str(context.exception))

        group = self.git.groups.list(search=group_name)[0]
        group.delete()

    def test_add_exist_subgroup(self):
        self.setUp()
        file_users = "../csv_test/test1_u.csv"
        file_prepods = "../csv_test/test1_p.csv"
        group_name = "Exist_group_Name_2130484272456"  # нужно создать группу
        subgroup_name = "Right_subgroup_Name_213dfg2"
        self.connection.set_add_g(group_name, file_prepods,
                                  file_users, subgroup_name)

        with self.assertRaises(Warning) as context:
            self.connection.create_group_and_proj()
        self.assertTrue('Успешное выполнение' in str(context.exception))

        with self.assertRaises(Warning) as context:
            self.connection.create_group_and_proj()
        self.assertTrue('Успешное выполнение' in str(context.exception))
        self.assertWarns(ResourceWarning)

        group = self.git.groups.list(search=group_name)[0]
        group.delete()


class TestAddSubject(unittest.TestCase):
    """
    Тесты для функции добавления предмета
    """
    def setUp(self):
        self.connection = Connection()
        self.connection.set_gitlab(TOKEN_)
        self.git = self.connection.ret_git()

    def test_wrong_name(self):
        self.setUp()
        sub_name = 'Wrong_sub_Name_21304000'
        group_name = 'Wrong_sub_Name_21304000'
        file_prepods = "../csv_test/test1_p.csv"
        with self.assertRaises(gitlab.GitlabSearchError) as context:
            self.connection.create_sub(sub_name, group_name, file_prepods)
        self.assertTrue(f"{group_name} не найден" in str(context.exception))

    def test_pass_add(self):
        self.setUp()
        group_name = 'Right_group_Name_2134564304872'  # нужно создать группу
        sub_grpup_name = 'Right_sub_Name_21304872'
        file_prepods = "../csv_test/empty_test.csv"
        file_users = "../csv_test/empty_test.csv"

        self.connection.set_add_g(group_name, file_prepods, file_users)
        with self.assertRaises(Warning):
            self.connection.create_group_and_proj()

        with self.assertRaises(Warning) as context:
            self.connection.create_sub(sub_grpup_name, group_name, file_prepods)
        self.assertTrue("Успешное выполнение" in str(context.exception))

        group = self.git.groups.list(search=group_name)[0]
        group.delete()

    def test_add_exist_subject(self):
        self.setUp()
        group_name = 'Right_group_Name_2136785687879'  # нужно создать группу
        sub_grpup_name = 'Right_sub_Name_21304872'
        file_prepods = "../csv_test/empty_test.csv"
        file_users = "../csv_test/empty_test.csv"
        self.connection.set_add_g(group_name, file_prepods, file_users)
        with self.assertRaises(Warning):
            self.connection.create_group_and_proj()
        with self.assertRaises(Warning) as context:
            self.connection.create_sub(sub_grpup_name, group_name, file_prepods)
            self.connection.create_sub(sub_grpup_name, group_name, file_prepods)
        self.assertTrue('Успешное выполнение' in str(context.exception))
        self.assertWarns(ResourceWarning)

        group = self.git.groups.list(search=group_name)[0]
        group.delete()

    def test_wrong_branch(self):
        self.setUp()
        group_name = 'Wrong_group_Name_21367856878796'
        sub_grpup_name = 'Right_sub_Name_21304872'
        file_prepods = "../csv_test/empty_test.csv"
        with self.assertRaises(gitlab.GitlabSearchError) as context:
            self.connection.create_sub(sub_grpup_name, group_name, file_prepods)
        self.assertTrue(f"{group_name} не найден" in str(context.exception))


class TestDeleteGroup(unittest.TestCase):
    """
    Тесты для функции delete_group. Проверены все исключение, успешное выполнение одной операции
    """
    def setUp(self):
        self.connection = Connection()
        self.connection.set_gitlab(TOKEN_)
        self.git = self.connection.ret_git()

    def test_wrong_g_name(self):
        self.setUp()
        group_name = "Wrong_group_Name_7902345346421"

        with self.assertRaises(IndexError) as context:
            self.connection.delete_group(group_name)
        self.assertTrue(f"{group_name} не найден" in str(context.exception))

    def test_wrong_sg_name(self):
        self.setUp()
        group_name = "Wrong_group_Name_8321041"
        subgroup_name = "Wrong_subgroup_Name_87812421"
        file_users = "../csv_test/test1_u.csv"
        file_prepods = "../csv_test/test1_p.csv"
        self.connection.set_add_g(group_name, file_prepods, file_users)
        with self.assertRaises(Warning) as context:
            self.connection.create_group_and_proj()
        self.assertTrue('Успешное выполнение' in str(context.exception))
        with self.assertRaises(IndexError) as context:
            self.connection.delete_group(group_name, subgroup_name)
        self.assertTrue(f"{subgroup_name} не найден" in str(context.exception))

        group = self.git.groups.list(search=group_name)[0]
        group.delete()

    def test_protect_g(self):
        self.setUp()
        group_name = "Protected_group_Name_549237493"

        with self.assertRaises(SystemError) as context:
            self.connection.delete_group(group_name)
        self.assertTrue('Группа защищена' in str(context.exception))

    def test_pass_del(self):
        self.setUp()
        group_name = "Right_group_Name_7321953732578"
        file_users = "../csv_test/empty_test.csv"
        file_prepods = "../csv_test/empty_test.csv"
        self.connection.set_add_g(group_name, file_prepods, file_users)
        with self.assertRaises(Warning) as context:
            self.connection.create_group_and_proj()
        self.assertTrue('Успешное выполнение' in str(context.exception))
        with self.assertRaises(Warning) as context:
            self.connection.delete_group(group_name)
        self.assertTrue(f"Группа {group_name} удалена успешно" in str(context.exception))

    def test_pass_del_sg(self):
        self.setUp()
        subgroup_name = 'Right_sub_Name_213048354'
        group_name = 'Right_branch_Name_213043468765'  # нужно создать группу
        file_prepods = "../csv_test/test1_p.csv"
        file_users = "../csv_test/test1_u.csv"

        self.connection.set_add_g(group_name, file_prepods, file_users, subgroup_name)
        with self.assertRaises(Warning) as context:
            self.connection.create_group_and_proj()
        self.assertTrue('Успешное выполнение' in str(context.exception))

        with self.assertRaises(Warning) as context:
            self.connection.delete_group(group_name, subgroup_name)
        self.assertTrue(f"Группа {subgroup_name} удалена успешно" in str(context.exception))

        group = self.git.groups.list(search=group_name)[0]
        group.delete()


class TestDeleteMember(unittest.TestCase):
    """
    Тесты для функции delete_members. Проверены все исключение, успешное выполнение одной операции
    """
    def setUp(self):
        self.connection = Connection()
        self.connection.set_gitlab(TOKEN_)
        self.git = self.connection.ret_git()

    def test_wrong_group(self):
        self.setUp()
        group_name = "Wrong_group_Name_123915"
        file_users = "../csv_test/test_s.csv"

        with self.assertRaises(IndexError) as context:
            self.connection.delete_members(group_name, file_users)
        self.assertTrue(f"{group_name} не найден" in str(context.exception))

    def test_wrong_subgroup(self):
        self.setUp()
        group_name = "Wrong_group_Name_93972114"
        subgroup_name = "Wrong_subgroup_Name_8345267"
        file_users = "../csv_test/test_s.csv"

        with self.assertRaises(IndexError) as context:
            self.connection.delete_members(group_name, file_users, subgroup_name)
        self.assertTrue(f"{group_name} не найден" in str(context.exception))

    def test_pass_delete(self):
        self.setUp()
        group_name = "Right_group_Name_9397211445623"  # нужно создать группу
        file_users = "../csv_test/test1_u.csv"
        file_prepods = "../csv_test/test1_p.csv"
        self.connection.set_add_g(group_name, file_prepods, file_users)
        with self.assertRaises(Warning) as context:
            self.connection.create_group_and_proj()
        self.assertTrue('Успешное выполнение' in str(context.exception))

        with self.assertRaises(Warning) as context:
            self.connection.delete_members(group_name, file_users)
        self.assertTrue(f"Пользователи удалены успешно из {group_name}" in str(context.exception))

        group = self.git.groups.list(search=group_name)[0]
        group.delete()


class TestChangeRole(unittest.TestCase):
    """
    Тесты для функции change_role.
    """

    def setUp(self):
        self.connection = Connection()
        self.connection.set_gitlab(TOKEN_)
        self.git = self.connection.ret_git()

    def test_chrole_group(self):
        self.setUp()
        file_users = "../csv_test/empty_test.csv"
        file_prepods = "../csv_test/test1_p.csv"
        group_name = "Right_group_Name_5472947500012"  # нужно создать группу
        sub_group_name = "test_pr408527414884"
        self.connection.set_add_g(group_name, file_prepods, file_users)
        with self.assertRaises(Warning) as context:
            self.connection.create_group_and_proj()
        self.assertTrue('Успешное выполнение' in str(context.exception))
        prepods = pd.read_csv(file_prepods, sep=";")
        # изменение роли для пользователя в группе
        with self.assertRaises(Warning):
            self.connection.change_role(prepods.login[0], group_name, "Owner")
        with self.assertRaises(Warning):
            self.connection.change_role(prepods.login[0], group_name, "GUEST")
        with self.assertRaises(Warning):
            self.connection.change_role(prepods.login[0], group_name, "Maintainer")
        with self.assertRaises(Warning):
            self.connection.change_role(prepods.login[0], group_name, "RePoRTer")
        with self.assertRaises(Warning):
            self.connection.change_role(prepods.login[0], group_name, "Owner")
        with self.assertRaises(Warning):
            self.connection.change_role(prepods.login[0], group_name, "DEVELOPER")
        with self.assertRaises(Warning):
            self.connection.delete_members(group_name, file_prepods)
        # изменение роли польователя для группы  внутри группы
        with self.assertRaises(Warning):
            self.connection.create_sub(sub_group_name, group_name, file_prepods)
        with self.assertRaises(Warning):
            self.connection.change_role(prepods.login[0], sub_group_name, "RePoRTer")
        with self.assertRaises(Warning):
            self.connection.change_role(prepods.login[0], sub_group_name, "Maintainer")
        with self.assertRaises(Warning):
            self.connection.change_role(prepods.login[0], sub_group_name, "GUEST")
        with self.assertRaises(Warning):
            self.connection.change_role(prepods.login[0], sub_group_name, "DEVELOPER")
        with self.assertRaises(Warning):
            self.connection.change_role(prepods.login[0], sub_group_name, "Owner")
        with self.assertRaises(Warning):
            self.connection.delete_group(group_name)

    def test_chrole_wrong_role(self):
        self.setUp()
        file_users = "../csv_test/empty_test.csv"
        file_prepods = "../csv_test/test1_p.csv"
        group_name = "Right_group_Name_8204638464646"  # нужно создать группу
        err_group_name = "error_group_name45476554434343"
        err_name = "error_name"
        self.connection.set_add_g(group_name, file_prepods, file_users)
        with self.assertRaises(Warning):
            self.connection.create_group_and_proj()
        # self.assertTrue('Успешное выполнение' in str(context.exception))
        prepods = pd.read_csv(file_prepods, sep=",")
        with self.assertRaises(ValueError):
            self.connection.change_role(prepods.login[0], group_name, "GUET")
        with self.assertRaises(ValueError):
            self.connection.change_role(prepods.login[0], group_name, "23456789")
        with self.assertRaises(Warning):
            self.connection.change_role(prepods.login[0], group_name, "GUEST")

        with self.assertRaises(AttributeError):
            self.connection.change_role(prepods.login[0], err_group_name, "GUEST")
        with self.assertRaises(IndexError):
            self.connection.change_role(err_name, group_name, "GUEST")

        with self.assertRaises(Warning):
            self.connection.delete_group(group_name)


class TestAddToGroups(unittest.TestCase):
    """
    Тесты для функции add_to_groups.
    """
    # pylint: disable=too-many-branches
    def setUp(self):
        self.connection = Connection()
        self.connection.set_gitlab(TOKEN_)
        self.git = self.connection.ret_git()

    def test_add_to_groups(self):
        self.setUp()
        prepod = TEST_PREPOD_NAME
        # все группы в файле нужно создать
        file_group_names = "../csv_test/group_names_test.csv"

        try:
            file_groups = pd.read_csv(file_group_names, sep=";")
        except FileNotFoundError as err_f:
            raise Exception(f"Не найден файл {err_f.filename}") from err_f

        role = "Owner"
        with self.assertRaises(Warning):
            self.connection.add_to_groups(prepod, role, file_group_names)
        for group in file_groups.name:
            for one_group in self.git.groups.list():
                if one_group.name == group:
                    one_group.members.delete(self.git.users.list(username=prepod)[0].id)

        role = "REpOrTeR"
        with self.assertRaises(Warning):
            self.connection.add_to_groups(prepod, role, file_group_names)
        for group in file_groups.name:
            for one_group in self.git.groups.list():
                if one_group.name == group:
                    one_group.members.delete(self.git.users.list(username=prepod)[0].id)

        role = "DEVELOPER"
        with self.assertRaises(Warning):
            self.connection.add_to_groups(prepod, role, file_group_names)
        for group in file_groups.name:
            for one_group in self.git.groups.list():
                if one_group.name == group:
                    one_group.members.delete(self.git.users.list(username=prepod)[0].id)

        role = "mAIntaiNer"
        with self.assertRaises(Warning):
            self.connection.add_to_groups(prepod, role, file_group_names)
        for group in file_groups.name:
            for one_group in self.git.groups.list():
                if one_group.name == group:
                    one_group.members.delete(self.git.users.list(username=prepod)[0].id)

        role = "guest"
        with self.assertRaises(Warning):
            self.connection.add_to_groups(prepod, role, file_group_names)
        for group in file_groups.name:
            for one_group in self.git.groups.list():
                if one_group.name == group:
                    one_group.delete()

    def test_add_to_groups_wrong(self):
        self.setUp()
        prepod = TEST_PREPOD_NAME
        uncorrect_prepod = "zqp016592jhg"
        file_group_names = "../csv_test/group_names_test.csv"  # все группы в файле нужно создать
        file_error_group_names = "../csv_test/group_errnames_test.csv"  # не нужно создавать группы
        not_exist_file = "../csv_test/does_not_exists_file4356256.csv"
        role = "Owner"
        uncorrect_role = "MAINAIN"
        try:
            file_groups = pd.read_csv(file_group_names, sep=";")
        except FileNotFoundError as err_f:
            raise Exception(f"Не найден файл {err_f.filename}") from err_f

        with self.assertRaises(FileNotFoundError):
            self.connection.add_to_groups(prepod, role, not_exist_file)
        with self.assertRaises(ValueError):
            self.connection.add_to_groups(prepod, uncorrect_role, file_group_names)
        with self.assertRaises(AttributeError):
            self.connection.add_to_groups(uncorrect_prepod, role, file_group_names)
        with self.assertRaises(NameError):
            self.connection.add_to_groups(prepod, role, file_error_group_names)
        with self.assertRaises(Warning):
            self.connection.add_to_groups(prepod, role, file_group_names)
        with self.assertRaises(gitlab.GitlabCreateError):
            self.connection.add_to_groups(prepod, role, file_group_names)

        for group in file_groups.name:
            for one_group in self.git.groups.list():
                if one_group.name == group:
                    one_group.delete()


if __name__ == '__main__':
    unittest.main()
