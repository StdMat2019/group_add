# Назначение программного средства
 
 Настоящее программное средство позволяет на сервисе *gitlab*: 

 * Создавать группу, подгруппу, проекты в ней с добавлением участников и назначением прав.
   
 * Удалять группу, подгруппу.
   
 * Удалять список слушателей из группы, подгруппы.

 * Изменять роль пользователя.

 * Добавить пользователя в список групп.

	

# Сборка и запуск 
Установка всех необходимых пакетов

    pip install -r requirements.txt
# Запуск pylint

    pylint main.py - pylint для main
---
    pylint gitlab_connection.py - pylint для методов
---
    pylint functions.py - pylint для функций
# Запуск тестов

    python test_runner.py <аргументы> --token <токен>

### Возможные аргументы:
    --all - запуск всех тестов
***
    --group_add - запуск тестов по созданию групп и подгрупп
***
    --group_del - запуск тестов по удалению групп и подгрупп
***    
    --group_delm - запуск тестов по удалению пользователей
***
    --sub_add    - запуск тестов по добавлению предмета
***
    --chrole    - изменение роли пользователя
***
    --groups_add    - добавление пользователя в список групп
# Запуск cli 

    usage: main.py [-h] [-t TOKEN] [-n NAME] [-s SUBNAME] [-u USERS] [-p PREPODS] [-g] [-m MILESTONE] [-i INSIDE] [--group_del] [--sub_add] [--group_delm] [--group_add] [--chrole] [--groups_add] [-R ROLE] [-U USER] [-G groups]


### Опциональные аргументы:

Show this help message and exit

    -h, --help

Input key for work

    -t TOKEN, --token TOKEN

Input name of main group

    -n NAME, --name NAME

Input name of subgroup

    -s SUBNAME, --subname SUBNAME
                        
Input dir for users

    -u USERS, --users USERS

Input  user name

    -U USER, --user USER
    
Input role

    -R ROLE --role ROLE

Input dir for groups

    -G GROUPS --groups GROUPS

Input dir for prepods

    -p PREPODS, --prepods PREPODS
                        
Add protect for group

    -g, --guarded         

Add milestones for projects. Format DD-MM-YY

    -m MILESTONE, --milestone MILESTONE

Make a kidp group
    
    -k KIDP, --kidp KIDP
                        
  
    
###Аргументы функций
Delete group

    --group_del           
Delete subgroup

    --group_dels          
Delete members from group/subgroup

    --group_delm          
Add group. May be secured

    --group_add           
Add people to group list.

    --groups_add           
Change role in the group

    --chrole           
Add subject. May be secured

    --sub_add

# Запуск веб-варианта

```bash
python3 main.py
```
Веб вариант будет работать по адресу: http://127.0.0.1:5000/grouper/login

# Запуск докер

**Создание докер образа** 
```bash
sudo docker build -t grouper:v0.1 .
```
-t repository:tag - создание образа с тегом

**Запуск докер образа с параметрами для работы через CLI:**

```bash
sudo docker run -it --rm grouper:v0.1 --token <токен> --group_del -n Test_group
```
--rm - созданый образ будет удален поле завершения работы

--it - подключение интерактивного tty в контейнер

**Для возможности использовать файлы из хоста в докере нужно запустить докер с монтированием папки/файлов:**
```bash
sudo docker run -v /path/on/host:/path/on/docker --rm grouper:v0.1 --group_add -t token -n new_group -u ./data/1.csv -p ./data/2.csv -k 0
```
-v - монтирование пути с хоста (/path/on/host) в путь в докере (/path/on/docker)

*нужно отметить что все файлы в папке "host" заменят файлы в "docker"
(те /path/on/host:/app заменит содержимое образа на содаржимое папки)*


**Для запука через графический интерфейс**
```bash
sudo docker run -v /path/on/host:/path/on/docker --rm grouper:v0.1
```
**Веб вариант будет запущен по адресу: http://172.17.0.2:5000/grouper/login**

# Пример работы докера

```bash 
sudo docker build -t grouper:v0.1 .

sudo docker run -v /home/user/Desktop/grdata:/app/newdata -it --rm grouper:v0.1 --token nW85gp96SFoLqkq --group_add  -n 1234 -p /app/newdata/3.csv -u /app/newdata/4.csv -k 0
```
после монтирования /home/user/Desktop/grdata:/app/newdata полнывй путь к файлам будет выглядеть /app/newdata/filename даже для веб варианта


# Примеры запуска

Добавление группы

    main.py --group_add -t token -n new_group -u ./data/1.csv -p ./data/2.csv

Удаление группы

    main.py --group_del -t token -n old_group

Удаление пользователей из подгруппы

    main.py --group_delm -t token -n old_group -s old_subgroup

Добавление предмета

    main.py --sub_add -t token -n branch -s sub_name


Добавление пользователя в группу проектов

	main.py --groups_add -t token -U new_pepole  -G ./data/1.csv -R role_in_groups

Изменение роли

	main.py --chrole -t token -U pepole  -G  -R new_role -n group


