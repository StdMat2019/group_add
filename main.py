#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Программа для обработки групп на сервисе gitwork.ru
"""

from functools import wraps
import argparse
import logging
import sys
from flask import Flask, render_template, request, flash, url_for, redirect, session

from scripts import args
from scripts.gitlab_connection import Connection
from scripts.functions import *
app = Flask(__name__)
app.config.from_json('microservice_cfg.json')
app.secret_key = '0adeaf40-1196-49b4-8b13-7d9cc68b005c'
SITE = 'https://gitlab.com/'


# pylint: disable=missing-function-docstring, broad-except, redefined-outer-name, lost-exception
# pylint: disable= broad-except
# Отключено, т.к. ловим любую ошибку и выводим сообщение о ней

# Запускается через http://127.0.0.1:5000/grouper/login (Правило sms-приложения)
@app.route('/grouper/')
def begin():
    """ start """
    return render_template("home.html")


# pylint: disable=invalid-name
def is_logged_in(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        flash('Авторизуйся!', 'danger')
        return redirect(url_for('login'))
    return wrap
# pylint: enable=invalid-name


@app.route('/grouper/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':

        token = request.form['token']
        try:

            git.set_gitlab(token)
            session['logged_in'] = True
            msg = 'Успешный вход'
            return render_template('home.html', msg=msg)

        except Exception as err_e:
            return render_template('login.html', error=err_e)
    return render_template('login.html')


@app.route('/grouper/logout/')
@is_logged_in
def logout():
    session.clear()
    flash('Успешный выход', 'success')
    return redirect(url_for('begin'))


@app.route('/grouper/meth/group_add/', methods=['GET', 'POST'])
@is_logged_in
def group_add():
    """
    Returns: возвращает на страницу с выводом состояния
    Raises:
        FileNotFoundError - ошибка поиска файла
        ValueError - неправильно названа группа/подгруппа
    """
    if request.method == 'GET':
        return render_template("group_add.html")

    group = request.form.get("group_name")
    subgroup = request.form.get("subgroup_name")
    file_prepods = request.form.get("file_prepods")
    file_users = request.form.get("file_users")
    secured = request.form.get("secured")
    milestone = request.form.get("milestone")
    kidp = request.form.get("kidp")
    result = request.form.get("result")

    try:
        git.set_add_g(group, file_prepods, file_users, subgroup)
        git.set_add_o(milestone, kidp, result)
        check_secure(secured, group)
        git.create_group_and_proj()
    except Warning as check:
        flash(str(check), category='success')
    except Exception as err_s:
        flash(str(err_s), category='danger')
    finally:
        return redirect(url_for('group_add'))


@app.route('/grouper/meth/groups_add/', methods=['GET', 'POST'])
@is_logged_in
def groups_add():
    """
    Returns: возвращает на страницу с выводом состояния
    Raises:
        FileNotFoundError - ошибка поиска файла
        ValueError - неправильно названа группа/подгруппа
    """
    if request.method == 'GET':
        return render_template("groups_add.html")

    user = request.form.get("user")
    file_groups_name = request.form.get("file_groups_name")
    role = request.form.get("role")

    try:
        git.add_to_groups(user, role, file_groups_name)

    except Warning as check:
        flash(str(check), category='success')
    except Exception as err_n:
        flash(str(err_n), category='danger')
    finally:
        return redirect(url_for('groups_add'))


@app.route('/grouper/meth/chrole/', methods=['GET', 'POST'])
@is_logged_in
def chrole():

    """
    Returns: возвращает на страницу с выводом состояния
    Raises:
        Exception - обработка всех исключений, вызываемых функцией
        Warning - успешное выполнение
    """

    if request.method == 'GET':
        return render_template("chrole.html")

    group = request.form.get("group_name")
    user = request.form.get("user_id")
    role = request.form.get("role")
    try:
        git.change_role(user, group, role)
    except Warning as check:
        flash(str(check), category='success')
    except Exception as err_s:
        flash(str(err_s), category='danger')
    finally:
        return redirect(url_for('chrole'))


@app.route('/grouper/meth/sub_add/', methods=['GET', 'POST'])
@is_logged_in
def sub_add():
    """
        Returns: возвращает на страницу с выводом состояния
        Raises:
            FileNotFoundError - ошибка поиска файла
    """
    if request.method == 'GET':
        return render_template("sub_add.html")

    sub_name = request.form.get("sub_name")
    group_name = request.form.get("group_name")
    file_prepods = request.form.get("file_prepods")
    secured = request.form.get("secured")

    try:
        check_secure(secured, sub_name)
        git.create_sub(sub_name, group_name, file_prepods)
    except Warning as check:
        flash(str(check), category='success')
    except Exception as err_e:
        flash(str(err_e), category='danger')
    finally:
        return redirect(url_for('sub_add'))


@app.route('/grouper/meth/group_del/', methods=['GET', 'POST'])
@is_logged_in
def group_del():
    """
    Удаление группы
    """

    if request.method == 'GET':
        return render_template("group_del.html")

    group_name = request.form.get("group_name")
    subgroup_name = request.form.get("subgroup_name")

    try:
        git.delete_group(group_name, subgroup_name)
    except Warning as check:
        flash(str(check), category='success')
    except Exception as check:
        flash(str(check), category='danger')
    finally:
        return redirect(url_for('group_del'))


@app.route('/grouper/meth/group_delm/', methods=['GET', 'POST'])
@is_logged_in
def group_delm():
    """
    Удаление пользователей из группы
    """
    if request.method == 'GET':
        return render_template("group_delm.html")

    group = request.form.get("name_group")
    file_users = request.form.get("file_users")

    try:
        git.delete_members(group, file_users,)
    except Warning as check:
        flash(str(check), category='success')
    except Exception as err_f:
        flash(str(err_f), category='danger')
    finally:
        return redirect(url_for('group_delm'))


if __name__ == '__main__':

    logging.basicConfig(level=logging.INFO)
    PARSER = argparse.ArgumentParser(description='GitWork_Create_group_and_projects')
    ARGS = args.parse_args(PARSER)

    GROUP = ARGS.name
    SUBGROUP = ARGS.subname
    TOKEN = ARGS.token
    GUARD = ARGS.guarded
    KIDP = ARGS.kidp
    RESULTS = ARGS.results
    MILESTONE = ARGS.milestone
    DEL_INSIDE = ARGS.inside
    FILE_USERS = ARGS.users
    FILE_PREPODS = ARGS.prepods
    ROLE = ARGS.role
    FILE_GROUPS = ARGS.groups
    USER = ARGS.user
    # pylint: disable=broad-except
    # Отключено, т.к. ловим любую ошибку и выводим сообщение о ней
    try:
        git = Connection()
        # with w.catch_warnings(record=True) as warnings:
        #     w.simplefilter("always")
        if TOKEN:
            git.set_gitlab(TOKEN)

            if ARGS.group_add and TOKEN:
                if ARGS.guarded:
                    protect_group(GROUP)

                git.set_add_g(GROUP, FILE_PREPODS, FILE_USERS, SUBGROUP)
                git.set_add_o(MILESTONE, KIDP, RESULTS)
                git.create_group_and_proj()

            if ARGS.group_del:
                git.delete_group(GROUP, SUBGROUP)

            if ARGS.sub_add:
                git.create_sub(SUBGROUP, GROUP, FILE_PREPODS)

            if ARGS.group_delm:
                git.delete_members(GROUP, FILE_USERS, SUBGROUP)

            if ARGS.groups_add:
                git.add_to_groups(USER, ROLE, FILE_GROUPS)

            if ARGS.chrole:
                git.change_role(USER, GROUP, ROLE)

    except Exception as err:
        sys.exit(err)

    else:
        git = Connection()
        # http://127.0.0.1:5000/grouper/login
        # 0.0.0.0 для docker
        app.run(host='0.0.0.0')
